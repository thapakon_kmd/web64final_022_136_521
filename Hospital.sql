-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 03, 2022 at 01:44 PM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `Appointment`
--

CREATE TABLE `Appointment` (
  `AppointmentID` int(11) NOT NULL,
  `PatientID` int(11) NOT NULL,
  `StaffID` int(11) NOT NULL,
  `AppointmentDate` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Appointment`
--

INSERT INTO `Appointment` (`AppointmentID`, `PatientID`, `StaffID`, `AppointmentDate`) VALUES
(1, 2, 1, '7/4/2565'),
(2, 3, 2, '8/4/2565'),
(3, 5, 3, '10/4/2565'),
(4, 5, 2, '11/4/2565'),
(6, 10, 2, '2022-04-03 09:04:49'),
(7, 5, 3, '5/4/2565'),
(8, 5, 2, '5/4/25666'),
(9, 5, 4, '8/4/256666666');

-- --------------------------------------------------------

--
-- Table structure for table `Patient`
--

CREATE TABLE `Patient` (
  `PatientID` int(11) NOT NULL,
  `PatientName` varchar(100) NOT NULL,
  `PatientSurname` varchar(100) NOT NULL,
  `PatientAge` int(11) NOT NULL,
  `Blood` varchar(10) NOT NULL,
  `Weight` float NOT NULL,
  `Height` float NOT NULL,
  `Username` varchar(100) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `CongenitalDisease` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Patient`
--

INSERT INTO `Patient` (`PatientID`, `PatientName`, `PatientSurname`, `PatientAge`, `Blood`, `Weight`, `Height`, `Username`, `Password`, `CongenitalDisease`, `Gender`) VALUES
(1, 'พิมพิศา', 'บุญนำ', 27, 'B', 50, 160, 'PimJa', '12345678', '-', 'หญิง'),
(2, 'หรรษา', 'สุขใจ', 68, 'A', 70, 175, 'Goh1', '12345678', 'โรคหัวใจ', 'ชาย'),
(3, 'สุชาติ', 'คงทน', 34, 'AB', 50, 171, 'Suchat23', '$2b$10$pj2zS3RGIsPPbLEz.PwYlu.KegiemOgwdM0blffIOlzFFIEfpC5CS', 'โรคไต', 'ชาย'),
(4, 'สมหญิง', 'ชนะรัตน์', 19, 'O', 50, 154, 'Somm12345', '$2b$10$WqeLCZoNgn73pYEOwTl9/.UI1TQTeAhck9V0MmgiqabdazCtmP.Ja', 'โรคภูมิแพ้', 'หญิง'),
(5, 'อำไพ', 'หวังดี', 67, 'A', 80, 176, 'Ooo123', '$2b$10$.zLhvhYWGf9pQ5XevTu//.6jfRdbqLansWsTAvlGb9Yg0vKCw4Dfy', 'โรคความดัน', 'ชาย'),
(7, 'สมพร', 'รุ่งเรือง', 63, 'AB', 81, 172, 'Porn123', '$2b$10$xCpXNXXIC3Db7rsF2h8Q4O.iw5UK2AwhjVNp7YDBFSuxOVHkvvA0.', 'โรคความดัน', 'หญิง'),
(10, 'ffggrf', 'fghff', 34, 'b', 23, 45, 'eiei214', '$2b$10$BX4glDVuH4CwLyG89U7Ms.Kf1thignMA0gWfdC8ONnAkLeP4QP21G', '-', 'ชาย'),
(15, 'testttt', 'testtttt', 34, 'b', 23, 45, 'eiei214', '$2b$10$2feihHKsFLarylqODILCVOLTvQXiV2tGMmn.zD.bMP7aR0AuEk2EG', '-', 'ชาย'),
(16, 'testttt1', 'testtttt1', 34, 'b', 23, 45, 'eiei214', '$2b$10$UxSQ2DseTMNVmDGBWfQkeeVQ5w4r19KzsTy9RTR42VPCLFIJiH7p2', '-', 'ชาย');

-- --------------------------------------------------------

--
-- Table structure for table `PatientRoom`
--

CREATE TABLE `PatientRoom` (
  `RoomID` int(11) NOT NULL,
  `RoomName` varchar(200) NOT NULL,
  `DailyPrice` float NOT NULL,
  `MonthlyPrice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `PatientRoom`
--

INSERT INTO `PatientRoom` (`RoomID`, `RoomName`, `DailyPrice`, `MonthlyPrice`) VALUES
(2201, 'ห้องพักเดี่ยว', 800, 15000),
(2202, 'ห้องพักเดี่ยว', 800, 15000),
(3301, 'ห้องพักคู่', 1600, 30000),
(3302, 'ห้องพักคู่', 1600, 30000),
(4302, 'หรูหรา', 3000, 90000);

-- --------------------------------------------------------

--
-- Table structure for table `RegistrationRoom`
--

CREATE TABLE `RegistrationRoom` (
  `RegistrationID` int(11) NOT NULL,
  `PatientID` int(11) NOT NULL,
  `RoomID` int(11) NOT NULL,
  `RegistrationTime` datetime NOT NULL,
  `StartDate` varchar(100) NOT NULL,
  `EndDate` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `RegistrationRoom`
--

INSERT INTO `RegistrationRoom` (`RegistrationID`, `PatientID`, `RoomID`, `RegistrationTime`, `StartDate`, `EndDate`) VALUES
(1, 1, 2201, '2022-03-26 07:10:25', '25/4/2565', '27/4/2565'),
(2, 3, 3301, '2022-04-01 15:08:58', '11/4/2565', '12/4/2565'),
(3, 4, 3302, '2022-04-01 15:14:31', '6/4/2565', '7/4/2565'),
(5, 10, 2201, '2022-04-02 17:40:49', '2022/05/27', '2022/04/08'),
(14, 5, 2201, '2022-04-03 06:23:22', '2022/04/04', '2022/04/05'),
(17, 5, 3302, '2022-04-03 11:38:59', '25/9/2565', '25/9/2565'),
(18, 10, 2202, '2022-04-03 11:50:00', '2022/04/02', '2022/04/02'),
(19, 5, 3302, '2022-04-03 12:14:08', '25/9/25655', '25/9/25655'),
(20, 10, 2202, '2022-04-03 12:24:05', '2022/05/12', '2022/05/14'),
(22, 5, 2202, '2022-04-03 12:26:57', '2022/07/14', '2022/07/21'),
(23, 5, 3302, '2022-04-03 12:43:06', '2022/09/22', '2022/09/30'),
(24, 5, 3301, '2022-04-03 12:47:55', '25/9/25655444', '25/9/25655444');

-- --------------------------------------------------------

--
-- Table structure for table `Staff`
--

CREATE TABLE `Staff` (
  `StaffID` int(11) NOT NULL,
  `StaffPrefix` varchar(50) NOT NULL,
  `StaffName` varchar(100) NOT NULL,
  `StaffSurname` varchar(100) NOT NULL,
  `Department` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Staff`
--

INSERT INTO `Staff` (`StaffID`, `StaffPrefix`, `StaffName`, `StaffSurname`, `Department`) VALUES
(1, 'พญ.', 'เจริญตา', 'ยศทรัพย์', 'แผนกหัวใจและหลอดเลือด'),
(2, 'นพ.', 'ฐาปกรณ์', 'กามูจันดี', 'แผนกคนหล่อ'),
(3, 'นพ.', 'วันเฉลิม', 'ลาภพูนทวี', 'แผนกอายุรกรรม'),
(4, 'พญ.', 'หทัยพรรณ', 'กาญจวิเศษ', 'แผนกกุมารเวชกรรม');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Appointment`
--
ALTER TABLE `Appointment`
  ADD PRIMARY KEY (`AppointmentID`),
  ADD KEY `PatientID1` (`PatientID`),
  ADD KEY `StaffID` (`StaffID`);

--
-- Indexes for table `Patient`
--
ALTER TABLE `Patient`
  ADD PRIMARY KEY (`PatientID`);

--
-- Indexes for table `PatientRoom`
--
ALTER TABLE `PatientRoom`
  ADD PRIMARY KEY (`RoomID`);

--
-- Indexes for table `RegistrationRoom`
--
ALTER TABLE `RegistrationRoom`
  ADD PRIMARY KEY (`RegistrationID`),
  ADD KEY `PatientID` (`PatientID`),
  ADD KEY `RoomID` (`RoomID`);

--
-- Indexes for table `Staff`
--
ALTER TABLE `Staff`
  ADD PRIMARY KEY (`StaffID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Appointment`
--
ALTER TABLE `Appointment`
  MODIFY `AppointmentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `Patient`
--
ALTER TABLE `Patient`
  MODIFY `PatientID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `RegistrationRoom`
--
ALTER TABLE `RegistrationRoom`
  MODIFY `RegistrationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `Staff`
--
ALTER TABLE `Staff`
  MODIFY `StaffID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Appointment`
--
ALTER TABLE `Appointment`
  ADD CONSTRAINT `PatientID1` FOREIGN KEY (`PatientID`) REFERENCES `Patient` (`PatientID`) ON DELETE CASCADE,
  ADD CONSTRAINT `StaffID` FOREIGN KEY (`StaffID`) REFERENCES `Staff` (`StaffID`) ON DELETE CASCADE;

--
-- Constraints for table `RegistrationRoom`
--
ALTER TABLE `RegistrationRoom`
  ADD CONSTRAINT `PatientID` FOREIGN KEY (`PatientID`) REFERENCES `Patient` (`PatientID`) ON DELETE CASCADE,
  ADD CONSTRAINT `RoomID` FOREIGN KEY (`RoomID`) REFERENCES `PatientRoom` (`RoomID`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
