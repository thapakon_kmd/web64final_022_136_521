//bcrypt
const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

//login
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')
const connection = mysql.createConnection({
    host : 'localhost',
    user : 'Naimong',
    password : '022136521',
    database : 'Hospital'
})

connection.connect();

const express = require('express')
const req = require('express/lib/request');
const res = require('express/lib/response');
const app = express()
const port = 4000
const cors = require('cors');
const { restart } = require('nodemon')
app.use(cors())

/* Middleware for Authenticating User Token */
function authenticateToken(req , res , next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET , (err , user) => {
        if (err) {return res.sendStatus(403)}
        else {
            req.user = user
            next()
        }
    })
}


//ข้อมูลห้องในเว็บ
app.get("/list_patientroom" , (req, res) => {
    let query = "SELECT * FROM PatientRoom";
    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})


//จองห้องพัก
app.post("/registrationroom", authenticateToken, (req, res) => {
    /*let user_profile = req.user*/
    // no need to req patient_id from web
    let patient_id = req.user.user_id
    let room_id = req.query.room_id
    let startdate = req.query.startdate
    let enddate = req.query.enddate

    let query = `INSERT INTO RegistrationRoom
                    ( PatientID,RoomID , StartDate, EndDate , RegistrationTime) 
                    VALUES (
                            ${patient_id},
                            ${room_id} ,
                            '${startdate.replaceAll("-", "/")}',
                            '${enddate.replaceAll("-", "/")}',
                             NOW() )`
    console.log(query)
    connection.query(query);
    // connection.query(query, (err, rows) => {
    //     if (err) {
    //         res.json({
    //             "status": "400",
    //             "message": "Error inserting data into db"
    //         })
    //     } else {
    //         res.json({
    //             "status": "200",
    //             "message": "Adding patient succesful"
    //         })
    //     }
    // });
});


//เพิ่มห้องพัก
app.post("/add_room", (req, res) => {

    let room_id = req.query.room_id
    let room_name = req.query.room_name
    let dailyprice = req.query.dailyprice
    let monthlyprice = req.query.monthlyprice

    let query = `INSERT INTO PatientRoom
                    (RoomID , RoomName , DailyPrice , MonthlyPrice) 
                    VALUES (${room_id} ,'${room_name}',${dailyprice} , ${monthlyprice} )`
    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error inserting data into db"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "Adding room succesful"
                    })
        }
    });


});


//ลบห้องพัก
app.post("/delete_patientroom", (req, res) => {

    let room_id = req.query.room_id

    let query = `DELETE FROM PatientRoom WHERE RoomID=${room_id}`

    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error deleting record"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "deleting room successful"
                    })
        }
    });
})


//อัพเดตห้องพัก
app.post("/update_patientroom", (req, res) => {

    let room_id = req.query.room_id
    let room_name = req.query.room_name
    let dailyprice = req.query.dailyprice
    let monthlyprice = req.query.monthlyprice

    let query = `UPDATE PatientRoom SET 
                    RoomName='${room_name}',
                    DailyPrice=${dailyprice},
                    MonthlyPrice=${monthlyprice}
                    WHERE RoomID=${room_id}`

    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error updating record"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "Updating room successful"
                    })
        }
    });
})


//ลิสต์การจองห้องของผู้ป่วยที่จองทั้งหมด
app.get("/list_registration" , (req, res) => {
    let query = `
    SELECT Patient.PatientID , Patient.PatientName , Patient.PatientSurname , 
           PatientRoom.RoomID , PatientRoom.RoomName , 
           RegistrationRoom.StartDate , RegistrationRoom.EndDate , RegistrationRoom.RegistrationTime

           FROM Patient , PatientRoom , RegistrationRoom

           WHERE (RegistrationRoom.PatientID = Patient.PatientID) AND 
           
           (RegistrationRoom.RoomID = PatientRoom.RoomID);`;

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})


//ดูข้อมูลการจองของแต่ละห้อง
app.get("/list_reg_room" , (req, res) => {

    let room_id = req.query.room_id
    let query = `
    SELECT Patient.PatientID , Patient.PatientName , Patient.PatientSurname ,  
           RegistrationRoom.StartDate , RegistrationRoom.EndDate , RegistrationRoom.RegistrationTime  
           FROM Patient , RegistrationRoom , PatientRoom 

           WHERE (RegistrationRoom.PatientID = Patient.PatientID) AND 
                 (RegistrationRoom.RoomID = PatientRoom .RoomID) AND
                 (PatientRoom.RoomID = ${room_id});`

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})

//ดูข้อมูลการจองห้องของคนไข้แต่ละคน
app.get("/list_regroom_person" , (req, res) => {

    let patient_id = req.query.patient_id
    let query = `
    SELECT Patient.PatientName , Patient.PatientSurname ,  
           RegistrationRoom.StartDate , RegistrationRoom.StartDate , RegistrationRoom.EndDate , RegistrationRoom.RegistrationTime ,
           PatientRoom.RoomName
           FROM Patient , RegistrationRoom , PatientRoom

           WHERE (RegistrationRoom.PatientID = Patient.PatientID) AND 
                 (RegistrationRoom.RoomID = PatientRoom.RoomID) AND
                 (Patient.PatientID = ${patient_id});`

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})


//นัดหมอ
app.post("/appointment", authenticateToken, (req, res) => {
    /*let user_profile = req.user*/
    let patient_id = req.user.user_id
    let staff_id = req.query.staff_id
    let date = req.query.date

    let query = `INSERT INTO Appointment
                    (PatientID , StaffID ,  AppointmentDate) 
                    VALUES (${patient_id} ,
                            ${staff_id} ,
                            '${date}' )`
    console.log(query)

    connection.query(query, (err, rows) => {
        if (err) {
            res.json({
                "status": "400",
                "message": "Error inserting data into db"
            })
        } else {
            res.json({
                "status": "200",
                "message": "Adding appointment succesful"
            })
        }
    });
});


//ลิสต์คนไข้ทั้งหมดที่นัดหมอ
app.get("/list_appoint" , (req, res) => {
    let query = `
    SELECT Patient.PatientID , Patient.PatientName , Patient.PatientSurname , 
           Staff.StaffPrefix ,Staff.StaffName ,Staff.StaffSurName , 
           Appointment.AppointmentDate

           FROM Patient , Staff , Appointment 

           WHERE (Appointment.PatientID = Patient.PatientID) AND 
           
           (Appointment.StaffID = Staff.StaffID);`;

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})

//ดูข้อมูลการนัดของหมอแต่ละคน
app.get("/list_appointdoctor" , (req, res) => {

    let staff_id = req.query.staff_id
    let query = `
    SELECT Staff.StaffPrefix , Staff.StaffName , Staff.StaffSurname ,  
           Patient.PatientID ,
           Appointment.AppointmentDate
           FROM Patient , Appointment ,Staff

           WHERE (Appointment.PatientID = Patient.PatientID) AND 
                 (Appointment.StaffID = Staff.StaffID) AND
                 (Staff.StaffID = ${staff_id});`

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})

//ดูข้อมูลการนัดของคนไข้แต่ละคน
app.get("/list_appointpatient" , (req, res) => {

    let patient_id = req.query.patient_id
    let query = `
    SELECT Patient.PatientName , Patient.PatientSurname , Patient.CongenitalDisease ,
           Staff.StaffPrefix , Staff.StaffName , Staff.StaffSurname ,
           Appointment.AppointmentDate
           FROM Patient , Appointment ,Staff

           WHERE (Appointment.PatientID = Patient.PatientID) AND 
                 (Appointment.StaffID = Staff.StaffID) AND
                 (Patient.PatientID  = ${patient_id});`

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})


//ลงทะเบียนผู้ป่วยใหม่
app.post("/register_patient" , (req , res) => {

    let patient_name = req.query.patient_name
    let patient_surname = req.query.patient_surname
    let patient_age = req.query.patient_age
    let blood = req.query.blood
    let weight = req.query.weight
    let height = req.query.height
    let congenitaldisease = req.query.congenitaldisease
    let gender = req.query.gender
    let patient_username = req.query.patient_username
    let patient_password = req.query.patient_password
    
    bcrypt.hash(patient_password, SALT_ROUNDS, (err , hash) => {
        let query = `INSERT INTO Patient
                (PatientName , PatientSurname, PatientAge , Blood , Weight , Height , 
                    CongenitalDisease , Gender , Username , Password) 
                VALUES ('${patient_name}' ,'${patient_surname}', ${patient_age}, 
                        '${blood}', ${weight}, ${height}, '${congenitaldisease}', '${gender}', 
                        '${patient_username}',  '${hash}')`
        console.log(query)
        connection.query(query, (err , rows) => {
        if (err) {
        res.json({ 
                "status" : "400",
                "message" : "Error inserting data into db"
                })
        }else {
        res.json({ 
                "status" : "200",
                "message" : "Adding new patient succesful"
                })
                }
        });
    })
});

//login
app.post("/login", (req, res) => {
    let username = req.query.username
    let user_password = req.query.password
    console.log(username, user_password)
    let query = `SELECT * FROM Patient WHERE Username='${username}'`
    try {
        connection.query(query, (err, rows) => {
            //There is no user
            if (!rows[0]) {
                res.send({
                    "status": "400",
                    "message": "Username not found!"
                })
            } else if (err) {
                console.log(err)
            } else {
                let db_password = rows[0].Password
                bcrypt.compare(user_password, db_password, (err, result) => {
                    if (result) {
                        let payload = {
                            "username": rows[0].Username,
                            "user_id": rows[0].PatientID,
                        }
                        console.log(payload)
                        let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: '1d' })

                        res.send({
                            "status": "200",
                            "token": token
                        })

                    } else {
                        res.send("Invalid username / password")
                    }
                })
            }

        })
    } catch (err) {
        console.log(err);
    }
})

//ลิสต์คนไข้
app.post("/list_patient" , (req, res) => {
    let query = "SELECT * FROM Patient";
    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})

//ลบข้อมูลคนไข้
app.post("/delete_patient", (req, res) => {

    let patient_id = req.query.patient_id

    let query = `DELETE FROM Patient WHERE PatientID=${patient_id}`

    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error deleting record"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "deleting patient successful"
                    })
        }
    });
})


// อัพเดตข้อมูลคนไข้
app.post("/update_patient", (req, res) => {

    let patient_id = req.query.patient_id
    let patient_name = req.query.patient_name
    let patient_surname = req.query.patient_surname
    let patient_age = req.query.patient_age
    let blood = req.query.blood
    let weight = req.query.weight
    let height = req.query.height
    let congenitaldisease = req.query.congenitaldisease
    let gender = req.query.gender

    let query = `UPDATE Patient SET 
                    PatientName='${patient_name}',
                    PatientSurName='${patient_surname}',
                    PatientAge=${patient_age},
                    Blood='${blood}',
                    Weight=${weight},
                    Height=${height},
                    Congenitaldisease='${congenitaldisease}',
                    Gender='${gender}'
                    WHERE PatientID=${patient_id}`

    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error updating record"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "Updating patient successful"
                    })
        }
    });
})


//ดูข้อมูลวันที่นัดหมอของคนไข้แต่ละคน
app.get("/list_appointperson" , (req, res) => {

    let patient_id = req.query.patient_id
    let query = `
    SELECT Patient.PatientName , Patient.PatientSurname ,  Patient.PatientAge ,
           Appointment.AppointmentDate , Appointment.StaffID
           FROM Patient , Appointment 

           WHERE (Appointment.PatientID = Patient.PatientID) AND
                 (Patient.PatientID = ${patient_id});`

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})


//ลิสต์ข้อมูลหมอ
app.get("/list_doctor" , (req, res) => {
    let query = "SELECT * FROM Staff";
    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error querying from hospital db"
                    })
        }else {
            res.json(rows)
        }
    });

})


//เพิ่มข้อมูลหมอ
app.post("/add_doctor", (req, res) => {

    let staff_prefix = req.query.staff_prefix
    let staff_name = req.query.staff_name
    let staff_surname = req.query.staff_surname
    let department= req.query.department
    

    let query = `INSERT INTO Staff
                    (StaffPrefix , StaffName , StaffSurname , Department ) 
                    VALUES ('${staff_prefix}', '${staff_name}','${staff_surname}' , '${department}' )`
    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            res.json({ 
                    "status" : "400",
                    "message" : "Error inserting data into db"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "Adding doctor succesful"
                    })
        }
    });


});


//ลบข้อมูลหมอ
app.post("/delete_doctor", (req, res) => {

    let staff_id = req.query.staff_id

    let query = `DELETE FROM Staff WHERE StaffID=${staff_id}`

    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error deleting record"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "deleting doctor successful"
                    })
        }
    });
})


//อัพเดตข้อมูลหมอ
app.post("/update_doctor", (req, res) => {

    let staff_id = req.query.staff_id
    let staff_prefix = req.query.staff_prefix
    let staff_name = req.query.staff_name
    let staff_surname = req.query.staff_surname
    let department = req.query.department
    

    let query = `UPDATE Staff SET 
                    StaffPrefix='${staff_prefix}',
                    StaffName='${staff_name}',
                    StaffSurname='${staff_surname}',
                    Department='${department}'
                    WHERE StaffID=${staff_id}`

    console.log(query)

    connection.query(query, (err , rows) => {
        if (err) {
            console.log(err)
            res.json({ 
                    "status" : "400",
                    "message" : "Error updating record"
                    })
        }else {
            res.json({ 
                    "status" : "200",
                    "message" : "Updating doctor successful"
                    })
        }
    });
})

app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port} `)
})




/*query = "SELECT * from Patient";
connection.query(query, (err , rows) => {
        if (err) {
            console.log(err);
        }else {
            console.log(rows);
        }
});

connection.end(); */