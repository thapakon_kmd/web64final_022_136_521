import './App.css';
import Header from './components/Header'
import Footer from './components/Footer'
import Home from './components/Home'
import Info from './components/hospital_info'
import Room from './components/Room'
import Register from './components/Registration'
import Doctor from './components/Doctor_info'
import SignIn from './components/Login'
import Booking from './components/Booking'
import Appointment from './components/Appointment'
import {Routes, Route} from "react-router-dom"


function App() {
  return (
    <div>
      <Header/>
        <div className="content">
          <Routes>
            <Route path="/" element={
              <Home/>
            }/>
            <Route path="/hospital_info" element={
              <Info/>
            }/>
            <Route path="/room" element={
              <Room/>
            }/>
            <Route path="/booking" element={
              <Booking/>
            }/>
            <Route path="/register" element={
              <Register/>
            }/>
            <Route path="/doctor_info" element={
              <Doctor/>
            }/>
            <Route path="/appointment" element={
              <Appointment/>
            }/>
            <Route path="/signin" element={
              <SignIn/>
            }/>
          </Routes>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
      <Footer/> 
    </div>
  );
}

export default App;
