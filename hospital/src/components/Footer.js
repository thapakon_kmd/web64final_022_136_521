import * as React from 'react';


function Footer() {
  return (
    <footer>
          <div className="info-footer-left">
            <h2 style={{color: "rgb(241, 196, 15)"}}>ที่อยู่</h2>
              โรงพยาบาลมหานครในหมง <br/>
              15 ถนน Brooklyn แขวงนิวซิตี้ <br/>
              เขตนิวในหมง มหานครในหมง <br/>
              รหัสไปรษณีย์ 99999 <br/>
          </div>

          <div className="info-footer-right">
            <h2 style={{color: "rgb(241, 196, 15)"}}>ข้อมูลติดต่อ</h2>
            โทรศัพท์ : 0-7445-5999 <br/>
            โทรสาร : 0-7421-2999, <br/>
            0-7421-2999 <br/>
            FB : Naimong city Hospital <br/>
            Line : @NMCHOSPITAL
          </div>
    </footer>
  );
}

export default Footer;