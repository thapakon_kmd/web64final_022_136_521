import * as React from 'react';
import Button from '@mui/material/Button';

function Header() {
  return (
    <div>
      
          <h1 style={{textAlign: "center"}}>โรงพยาบาลมหานครในหมง</h1>
      
        <center>
                <Button variant="text" href="/" size="large">หน้าหลัก</Button>
                <Button variant="text" href="/hospital_info" size="large">เกี่ยวกับเรา</Button>
                <Button variant="text" href="/room" size="large">ห้องพักผู้ป่วย</Button>
                <Button variant="text" href="/appointment" size="large">นัดตรวจ</Button>
                <Button variant="text" href="/doctor_info" size="large">ข้อมูลหมอและพยาบาล</Button>
                <Button variant="text" href="/register" size="large">ลงทะเบียนผู้ป่วยใหม่</Button>
                <Button variant="text" href="/SignIn" size="large">บัญชีผู้ใช้</Button>
        </center>
    </div>
  );
}

export default Header;
