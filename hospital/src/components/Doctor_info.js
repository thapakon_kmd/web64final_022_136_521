import React from 'react';
import pic from './images/doctor.png';
import './doctor_info.css';
import Button from '@mui/material/Button';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
    root: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      border: 0,
      borderRadius: 5,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px',
    },
});


function Doctor(){
    const classes = useStyles();
    return (
        <div className="container">
                <div className="flex-doctor">
                    <center>
                        <img src={pic} alt="Logo" style={{marginTop : "20px"}}/>
                        <p>Charoenta Yotsub</p>
                        <p>Job : Doctor</p>
                        <p>Department : Cardiovascular </p>
                    </center>
                </div>

                <div className="flex-doctor">
                    <center>
                        <img src={pic} alt="Logo" style={{marginTop : "20px"}}/>
                        <p>Thapakon Kamujandee</p>
                        <p>Job : Doctor</p>
                        <p>Department : Handsome </p>
                    </center>
                </div>

                <div className="flex-doctor">
                    <center>
                        <img src={pic} alt="Logo" style={{marginTop : "20px"}}/>
                        <p>Wanchalerm Lappoolthawee</p>
                        <p>Job : Doctor</p>
                        <p>Department : Internal Medicine </p>
                    </center>
                </div>

                <div className="flex-doctor">
                    <center>
                        <img src={pic} alt="Logo" style={{marginTop : "20px"}}/>
                        <p>Hathaiphan Kanjawiset</p>
                        <p>Job : Doctor</p>
                        <p>Department : Pediatrician</p>
                    </center>
                </div>

                

        </div>
    );
}

export default Doctor;
