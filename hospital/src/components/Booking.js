import * as React from 'react';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Button from '@mui/material/Button';
import pic from './images/booking.png';
import Select from '@mui/material/Select';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem'
import Stack from '@mui/material/Stack';
import { purple, pink } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import PopUp from './/PopUp'
import { useState, useEffect } from 'react'
import axios from 'axios';
const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: pink[500],
    '&:hover': {
        backgroundColor: pink[700],
    },
}));

const rooms = [
    {
        value: 'Single Room',
    },
    {
        value: 'Double Room',
    }
]

function Booking() {
    const [buttonPopup, setButtonPopup] = useState(false)
    const [startDate, setStartDate] = React.useState(new Date());
    const [endDate, setEndDate] = React.useState(new Date());
    const [roomType, setRoomType] = React.useState("");
    const [roomId, setRoomId] = React.useState();
    const [singleRoom] = React.useState(["2201", "2202"]);
    const [doubleRoom] = React.useState(["3301", "3302"]);
    const handleRoomType = (e) => {
        setRoomType(e.target.value);
    }
    const handleStartDate = (e) => {
        setStartDate(e.target.value);
    }
    const handleEndDate = (e) => {
        if (e.target.value < startDate) {
            console.log("End date < start date!")
        }
        setEndDate(e.target.value);
    }
    const handleRoomId = (e) => {
        setRoomId(e.target.value);
    }
    const handleBooking = async () => {
        console.log(sessionStorage.getItem("token"))
        const res = await axios.post(`http://127.0.0.1:4000/registrationroom?token=${sessionStorage.getItem("token")}&room_id=${roomId}&startdate=${startDate}&enddate=${endDate}`);
    // console.log(res.body.message);
}

useEffect(() => {
    // Check that user is login
    // if (!sessionStorage.getItem("token"))
    //     window.location.href = '/Signin';
});
return (
    <center>
        <br />
        <h2>จองห้องพัก</h2>
        <img src={pic} alt="Logo" style={{ borderRadius: "20px" }} />
        <br />
        <br />
        <Box sx={{ minWidth: 120 }}>
            <FormControl style={{ marginBottom: 20, width: 300 }}>
                <InputLabel id="demo-simple-select-label">เลือกประเภทห้องพัก</InputLabel>
                <Select
                    onChange={(e) => handleRoomType(e)}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    // value={room}
                    label="เลือกประเภทห้องพัก"
                >
                    <MenuItem value={"Single Room"}>ห้องพักเดี่ยว</MenuItem>
                    <MenuItem value={"Double Room"}>ห้องพักคู่</MenuItem>
                </Select>
            </FormControl>
        </Box>
        <br />
        <Box sx={{ minWidth: 120 }}>
            <FormControl style={{ marginBottom: 20, width: 300 }}>
                <InputLabel id="demo-simple-select-label">หมายเลขห้อง</InputLabel>
                <Select
                    onChange={(e) => handleRoomId(e)}
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                >
                    {roomType === "Single Room" &&
                        singleRoom.map(e =>
                            <MenuItem value={e}>{e}</MenuItem>
                        )
                    }
                    {roomType === "Double Room" &&
                        doubleRoom.map(e =>
                            <MenuItem value={e}>{e}</MenuItem>
                        )
                    }
                </Select>
            </FormControl>
        </Box>

        <Stack component="form" noValidate spacing={3}>
            <TextField
                onChange={(e) => handleStartDate(e)}
                id="date"
                label="วันที่เข้าพัก"
                type="date"
                sx={{ width: 300 }}
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <br />
            <TextField
                onChange={(e) => handleEndDate(e)}
                id="datetime-local"
                label="วันที่ออกจากห้องพัก"
                type="date"
                sx={{ width: 300 }}
                InputLabelProps={{
                    shrink: true,
                }}
            />
        </Stack>
        <br />
        <br />
        <ColorButton variant="contained" href="" size="large" onClick={() => handleBooking()}>ยืนยันการจอง</ColorButton>
        <PopUp trigger={buttonPopup} setTrigger={setButtonPopup}>
            <h3>ทำการจองสำเร็จ</h3>
        </PopUp>
        <br />
        <br />
        <Button variant="contained" href="/room" size="small">กลับไปยังหน้าจอง</Button>
    </center>
);
}



export default Booking;