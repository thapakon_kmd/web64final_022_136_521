import React from 'react';
import './Appointment.css';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import CalendarPicker from '@mui/lab/CalendarPicker';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import { purple, pink } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import MobileTimePicker from '@mui/lab/MobileTimePicker';



const minDate = new Date('2020-01-01T00:00:00.000');
const maxDate = new Date('2034-01-01T00:00:00.000');

const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: pink[500],
    '&:hover': {
      backgroundColor: pink[700],
    },
}));

function Appointment(){

    const [date, setDate] = React.useState(new Date());
    const [value, setValue] = React.useState(new Date());

    return (
        <div style={{marginLeft: "50px"}}>
            <h2 style={{marginLeft: "100px"}}>จองการนัดตรวจ</h2>
            <div className='container'>
                <div className='flex-appointment'>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <center><h3>เลือกวันที่ต้องการตรวจ</h3></center>
                        <Grid item xs={12} md={6}>
                            <CalendarPicker date={date} onChange={(newDate) => setDate(newDate)} />
                        </Grid>
                    </LocalizationProvider>
                </div>

                <div className='flex-appointment-right'>
                <center>
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: -1, width: '25ch'},
                        }}
                            noValidate
                            autoComplete="off"
                        >
                        <br/>
                        <TextField id="standard-basic" label="เหตุผลที่ต้องการนัดตรวจ" variant="standard" />
                    </Box>

                    <br/>

                    <div className='flex-appointment-time'>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <MobileTimePicker
                                label="เลือกเวลา"
                                value={value}
                                onChange={(newValue) => {
                                    setValue(newValue);
                                }}
                                renderInput={(params) => <TextField {...params} />}
                            />
                        </LocalizationProvider>
                    </div>
                    <br/><br/><br/><br/>
                        <ColorButton variant="contained" href="" size="large">ยืนยันการจอง</ColorButton>
                </center>
                </div>
            </div>
        </div>
    );
}

export default Appointment;
