import React from 'react';
import Button from '@mui/material/Button';
import pic from './images/bed.jpg';
import pic2 from './images/2bed.jpeg';
import { purple, pink } from '@mui/material/colors';
import { styled } from '@mui/material/styles';

const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: pink[500],
    '&:hover': {
      backgroundColor: pink[700],
    },
}));

function Room(){
    return (
        <div>
            <br/>
            <div className="flex-container" style={{marginRight: "50px", marginLeft: "50px"}}>
                    <div className="flex-item-left">
                        <center>
                            <h3>ห้องพักเดี่ยว</h3>
                            <img src={pic} alt="Logo" style={{borderRadius : "20px"}}/>
                            <h5>รายวัน 800 บาท / รายเดือน 15,000 บาท</h5>
                        </center>

                    </div>
                    <div className="flex-item-right">
                        <center>
                            <h3>ห้องพักคู่</h3>
                            <img src={pic2} alt="Logo" style={{borderRadius : "20px"}}/>
                            <h5>รายวัน 1,600 บาท / รายเดือน 30,000 บาท</h5>
                        </center>
                    </div>
            </div>
            <br/><br/>
            <center>
                <ColorButton variant="contained" href="/booking" size="large">ไปยังหน้าจอง</ColorButton>
            </center>
        </div>

    );
}

export default Room;
