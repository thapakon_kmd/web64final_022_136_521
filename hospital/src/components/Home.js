import Button from '@mui/material/Button';
import pic from './images/bed.jpg';
import pic2 from './images/2bed.jpeg';
import { purple, pink } from '@mui/material/colors';
import { styled } from '@mui/material/styles';

const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: pink[500],
    '&:hover': {
      backgroundColor: pink[700],
    },
}));

function Home(){
    return (
        <div> 
            <h2 style={{marginLeft: "50px"}}>ยินดีต้อนรับสู่โรงพยาบาลมหานครในหมง</h2>
            <br/>
            <div className="flex-container" style={{marginRight: "50px", marginLeft: "50px"}}>
                <div className="flex-item-left"> 
                    <center>
                        <h4>ประชาสัมพันธ์</h4>
                    </center>
                    <p> >>> ไม่อนุญาตให้เข้าเฝ้าผู้ป่วยทุกกรณี เนื่องจากสถานการณ์ COVID-19 </p>
                    <p> >>> สำหรับคนที่ติดเชื้อ COVID-19 ให้ไปอาคารอำนวยการของโรงพยาบาล</p>
                    <p> >>> สำหรับคนที่อาการอื่น ๆ ที่ต้องการได้รับการรักษา ห้ามเข้าไปที่อาคารอำนวยการเด็ดขาด</p>
                    <p> >>> Food Center ปิดทำการเป็นเวลา 1 เดือนเพื่อป้องกันการระบาด</p>
                </div>
                <div className="flex-item-right">
                    <center>
                        <h4>ห้องพัก</h4>
                            <div>
                                <img src={pic} alt="Logo" style={{borderRadius : "20px", width : "350px", padding : "10px"}}/>
                                <img src={pic2} alt="Logo" style={{borderRadius : "20px", width : "350px", padding : "10px"}}/>
                            </div>
                        <br/>
                        <ColorButton variant="contained" href="/room" size="medium">รายละเอียด</ColorButton>
                        <br/>
                        <br/>
                    </center>
                </div>
            </div>
            <br/>
            <br/>
            <center>
                <Button variant="contained" href="/hospital_info" size="large">รู้จักเรามากขึ้น</Button>
            </center>
        </div>
    );
}

export default Home;
