import React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';

const genders = [
    {
      value: 'male',
      label: 'ชาย',
    },
    {
      value: 'female',
      label: 'หญิง',
    },
];

const bloods = [
    {
        value: 'a',
        label: 'A',
    },
    {
        value: 'b',
        label: 'B',
    },
    {
        value: 'ab',
        label: 'AB',
    },
    {
        value: 'o',
        label: 'O',
    },
];

function Register() {
    const [gender, setGender] = React.useState('');
    const [blood, setBlood] = React.useState('');

    const handleChange = (event) => {
        setGender(event.target.value);
        setBlood(event.target.value);
    };

    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
        >
            <br/>
            <div style={{textAlign: "center"}} >
            <h3 style={{textAlign: "center"}}>ลงทะเบียนผู้ป่วยใหม่</h3>
                <TextField
                    required
                    id="outlined-required"
                    label="ชื่อ"
                />
                <TextField
                    required
                    id="outlined-required"
                    label="นามสกุล"
                />
                <br/>
                <TextField
                    id="outlined"
                    label="อายุ"
                    type="number"
                />
                <TextField
                    id="outlined-select-currency"
                    select
                    label="เพศ"
                    value={gender}
                    onChange={handleChange}
                    >
                    {genders.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                        {option.label}
                        </MenuItem>
                    ))}
                </TextField>
                <br/>
                <TextField
                    id="outlined-select-currency"
                    select
                    label="กรุ๊ปเลือด"
                    value={blood}
                    onChange={handleChange}
                    >
                    {bloods.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                        {option.label}
                        </MenuItem>
                    ))}
                </TextField>
                <TextField
                    id="outlined"
                    label="โรคประจำตัว"
                />
                <br/>
                <TextField
                    label="น้ำหนัก"
                    id="outlined-start-adornment"
                    sx={{ m: 1, width: '25ch' }}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">กก.</InputAdornment>,
                    }}
                />
                <TextField
                    label="ส่วนสูง"
                    id="outlined-start-adornment"
                    sx={{ m: 1, width: '25ch' }}
                    InputProps={{
                        startAdornment: <InputAdornment position="start">ซม.</InputAdornment>,
                    }}
                />
                <br/>
                <TextField
                    required
                    id="outlined-required"
                    label="ชื่อผู้ใช้"
                />
                <TextField
                    id="outlined-password-input"
                    label="ตั้งรหัสผ่าน"
                    type="password"
                    autoComplete="current-password"
                />
                <br/>
                <TextField
                    id="outlined-password-input"
                    label="ยืนยันรหัสผ่าน"
                    type="password"
                    autoComplete="current-password"
                />
                <br/>
                <br/>
                <br/>

                <Button variant="contained" href="" size="large">ส่งข้อมูล</Button>

            </div>
        </Box>
    );
}

export default Register;
